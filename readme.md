### 一些优秀项目的地址的收集

> mark一下，以备不时之需。

- `TLOG`一个轻量级的分布式日志标记追踪神器
  - https://gitee.com/bryan31/TLog
- `LeetCode`题目分类与面试问题整理
  - https://github.com/yuanguangxin/LeetCode
- `LeetCode`解题之路
  - https://github.com/azl397985856/leetcode/blob/master/README.md

#### dubbo

- https://gitee.com/apache/dubbo
- https://github.com/apache/dubbo-admin

#### 源码学习

- [netty](https://gitee.com/zengshunyao/learn_netty_source_code)
- [spring](https://github.com/seaswalker/spring-analysis)

### 工具

* `redis`客户端
  
  - https://github.com/qishibo/AnotherRedisDesktopManager
  
* IDEA技巧
  
  * https://github.com/xiaoxiunique/tool-tips
  
*  把平时有用的手动操作做成脚本，这样可以便捷的使用

  > 1. [show-busy-java-threads](https://github.com/oldratlee/useful-scripts/blob/master/docs/java.md#-show-busy-java-threads)
  >    用于快速排查`Java`的`CPU`性能问题(`top us`值过高)，自动查出运行的`Java`进程中消耗`CPU`多的线程，并打印出其线程栈，从而确定导致性能问题的方法调用。
  > 2. [show-duplicate-java-classes](https://github.com/oldratlee/useful-scripts/blob/master/docs/java.md#-show-duplicate-java-classes)
  >    找出`jar`文件和`class`目录中的重复类。用于排查`Java`类冲突问题。
  > 3. [find-in-jars](https://github.com/oldratlee/useful-scripts/blob/master/docs/java.md#-find-in-jars)
  >    在目录下所有`jar`文件里，查找类或资源文件。
  > 4. 等等

  * https://github.com/oldratlee/useful-scripts

